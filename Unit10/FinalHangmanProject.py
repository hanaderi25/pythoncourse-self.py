
#1



MAX_TRIES = 6
win = False
HANGMAN_ASCII_ART = """ Welcome to the game Hangman!
    _    _
   | |  | |
   | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __
   |  __  |/ _' | '_ \ / _' | '_ ' _ \ / _' | '_ \\
   | |  | | (_| | | | | (_| | | | | | | (_| | | | |
   |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                        __/ |
                       |___/

"""



#2


def choose_word(file_path, index):
    """The function accepts as a string parameter.
        The function returns true if the last character that appears in the string also appears before.
        Otherwise the function returns false."""
    global secret_word
    global new_list
    f = open(file_path,"r")
    new_list = []
    words_data = f.read().split()
    while index > len(words_data):
        index = index - len(words_data)
    secret_word = words_data[index-1]






#3
num_of_tries = 0

HANGMAN_PHOTOS = {
    
    0: """x-------x
|
|
|
|
|
          """,
    1: """x-------x
|       |
|       0
|
|
|
          """,
    2: """x-------x
|       |
|       0
|       |
|
|
          """,
    3: """x-------x
|       |
|       0
|      /|\\
|
|
          """,
    4: """x-------x
|       |
|       0
|      /|\\ 
|      /
| 
          """,
    5: """x-------x
|       |
|       0
|      |||
|      / \\
|
          """
}

def print_hangman(num_of_tries):
    """this function recives a number and prints the matching
        hangman photo from the global dictionary HANGMAN_PHOTOS

        param: num_of_tries: an integer"""
    print (HANGMAN_PHOTOS[num_of_tries])





#4
def secret_word_lines(secret_word):
    print ("_ " * len(secret_word))






#5

old_letters_guessed = ['']
letter_guessed = ''

def guess_a_letter():
    global letter_guessed 
    global old_letters_guessed
    letter_guessed = input("Guess a letter: ")
    letter_guessed = letter_guessed.lower()
    try_update_letter_guessed(letter_guessed)
    return letter_guessed


def try_update_letter_guessed(letter_guessed):
    """
    checkes if the old letters list can be updated, if so updates, else it prints "X" and the old letters list sorted in lower caps with " -> " in between
    Arguments:
    letter_guessed -- the string input from the user
    old_letters_guessed -- a list containing all the letters the user guessed before

    Returns:
    returns True if it can
    returns False if otherwise
    rtype: bool
    """
    global old_letters_guessed

    if len(letter_guessed) > 1:
        print ("X")
        old_letters_guessed.sort()
        print (" -> ".join(old_letters_guessed)[3:])
    

    elif not letter_guessed.isalpha():
        print ("X")
        old_letters_guessed.sort()
        print (" -> ".join(old_letters_guessed)[3:])
        

    elif letter_guessed in old_letters_guessed:
        print ("X")
        old_letters_guessed.sort()
        print (" -> ".join(old_letters_guessed)[3:])

    else:
        old_letters_guessed.append(letter_guessed)
        old_letters_guessed.sort()
        print (" -> ".join(old_letters_guessed)[3:])
       





#6
def show_hidden_word(secret_word, old_letters_guessed):
    
    for letter_guessed in secret_word:
        if letter_guessed in old_letters_guessed:
            print (letter_guessed + " ", end = '')
            
        else:
            print ("_ ", end = '')

    print()


  





#7
def check_win(secret_word, old_letters_guessed):
    """
        Check if all letters in secret_word are guessed by old_letters_guessed.

        Args:
        secret_word (str): A string representing the secret word.
        old_letters_guessed (list): A list of letters that were already guessed.

        Returns:
        bool: True if all letters in secret_word are guessed by old_letters_guessed, False otherwise.
        """
    count = 0;
    global win
  
    for letter_guessed in secret_word:
        
        if letter_guessed in old_letters_guessed:
            count+=1

    if count == len(secret_word):
        win =  True
        
    else:
        win =  False

    return win



def change_tries(letter_guessed, secret_word):
    global num_of_tries
    if letter_guessed not in secret_word:
        num_of_tries+=1





def main():
    print (HANGMAN_ASCII_ART, "Max tries: ", MAX_TRIES)
    global win
    
    
    file_path = input('Please enter file path: ')
    enter_index = int(input("Please enter index: "))
    choose_word(file_path, enter_index)

    secret_word_lines(secret_word)

    while num_of_tries < MAX_TRIES and not win:
        print_hangman(num_of_tries)

        letter_guessed = guess_a_letter()
        change_tries(letter_guessed, secret_word)
        show_hidden_word(secret_word, old_letters_guessed)

        win = check_win(secret_word, old_letters_guessed)

    
    

    if win:
        print ("Win!")

    else:
        print ("Lose!")


    








if __name__ == '__main__':
    main()